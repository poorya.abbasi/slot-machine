# Jackpot
This is a simple jackpot project utlizing Vue 3, Typescript, ...
### Prefred IDE Setup
Visual Studio Code + Volar Extension
### Local Development Guide
In order to run the project on your local machine execute the following commands.

`npm install`

`npm run dev`
### Testing
Component testing is done using Jest and Vue Test Utils.

There are tests written to ensure that the logic is working precisely as intended.

To run tests execute

`npm run test`

### Contact
For any further questions please contact pooryaa@icloud.com.
