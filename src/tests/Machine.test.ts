import { shallowMount } from "@vue/test-utils";
import Machine from "../components/Machine.vue";

describe("Machine Component", () => {
    const wrapper = shallowMount(Machine);
    it("Has 10 credits at first", () => {
        expect(wrapper.vm.credits).toBe(10);
    });
    it("Losing decreases 1 credit", () => {
        wrapper.vm.decideCredits(false);
        expect(wrapper.vm.credits).toBe(9);
    });
    it("Cherry jackpot adds 10 credits", () => {
        wrapper.vm.slots[0] = "cherry.svg";
        wrapper.vm.decideCredits(true);
        expect(wrapper.vm.credits).toBe(19);
    });
    it("Lemon jackpot adds 20 credits", () => {
        wrapper.vm.slots[0] = "lemon.svg";
        wrapper.vm.decideCredits(true);
        expect(wrapper.vm.credits).toBe(39);
    });
    it("Orange jackpot adds 30 credits", () => {
        wrapper.vm.slots[0] = "orange.svg";
        wrapper.vm.decideCredits(true);
        expect(wrapper.vm.credits).toBe(69);
    });
    it("Watermelon jackpot adds 40 credits", () => {
        wrapper.vm.slots[0] = "watermelon.svg";
        wrapper.vm.decideCredits(true);
        expect(wrapper.vm.credits).toBe(109);
    });
    it("Rolling repeatedly is not allowed", () => {
        wrapper.vm.roll();
        expect(wrapper.vm.roll()).toBe(0);
    });
    it("Reset button restores credits to 10", () => {
        wrapper.vm.reset();
        expect(wrapper.vm.credits).toBe(10);
    });
    it("Win check algorithm works correctly", () => {
        expect(wrapper.vm.isWinner(["X", "X", "X"])).toBe(true);
        expect(wrapper.vm.isWinner(["X", "Y", "X"])).toBe(false);
    });
});
